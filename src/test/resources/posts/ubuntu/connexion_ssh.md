Pour vous connecter avec une clé SSH sur un serveur linux vous devez suivre les instructions ci-dessous :

Pour des raisons de sécurité évidente, n’utiliser pas les clés publiques/privés générées ci-dessous.

Les commandes ci-dessous doivent être exécutées depuis l’utilisateur root

## putty

La connexion SSH nécessite un client SSH tel que [putty](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html) qui devra être installé sur le poste client qui devra se connecter sur le serveur linux.

## Génération du bi-clé SSH

La création de la clé SSH est réalisée par le programme puttygen.

![Ecran%20de%20puttygen](https://aticdnet.albumhd.fr/r/6940d5dd-7765-43f8-8c7c-83450d2b97a3.png)

Cliquer sur le bouton « Generate », vous devrez ensuite bouger votre souris pendant le processus de génération de la clé.

![Génération%20du%20bi-clé%20SSH](https://aticdnet.albumhd.fr/r/cf4a54f6-a1ef-4a14-ab89-2030ab0f43f1.png)

Il faut maintenant copier le contenue de la clé publique au format ssh-rsa.

![Copie%20de%20la%20clé%20publique](https://aticdnet.albumhd.fr/r/d7514d14-8d9c-4c6c-a33e-1e7301c53373.png)

## Installation de la clé publique SSH

Remplacer {username} par le nom de votre utilisateur.

Remplacer {votre_ip_client} par l’adresse IP, ou les adresses IP séparés par une virgule, de l’ordinateur qui devra se connecter sur ce serveur.

Le fichier contenant toutes les clés publiques SSH se trouve sous /root/.ssh/authorized_keys2 pour l’utilisateur root et sous /home/{username}/.ssh/authorized_keys2 pour l’utilisateur {username}.

Edition du fichier authorized_keys2 :

```bash
vi /home/{username}/.ssh/authorized_keys2
				
```

Copier la clé publique au format rss-dsa dans le fichier authorized_keys2 en rajoutant au préalable au besoins from= »{votre_ip_client} » pour restreindre l’utilisation de cette clé depuis l’ordinateur possédant l’adresse IP {votre_ip_client}

Ce fichier authorized_keys2 peut contenir plusieurs clés SSH

```bash
from="{votre_ip_client}" ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAIBVOZsEuEsXIa+SygY8bQh2YmFZ9E0U0yvqez3R2JLSNjovv8VMN5pm90+ddnF7blwSHbCOMvMCY8EfA0X+p+UoQVXEBxGx06T2If8uzRDeoZyycH0A/WOKm9VJiydT4ZW5VUT9XsIdIdZCPRnq1tVJflouL6EPZb4w20k1+4xuHw== rsa-key-20140704
				
```

## Paramétrage du daemon SSH

Editer le fichier sshd_config.

```bash
vi /etc/ssh/sshd_config
				
```

Les clés ci-dessous doivent être mises à « yes »

*RSAAuthentication yes*

*PubkeyAuthentication yes*

Recharger la configuration

```bash
/etc/init.d/ssh reload
				
```

## Sauvegarde de la clé privée SSH

Remplir les champs « Key passphrase » et « Confirm passphrase » avec un mot de passe permettant de protéger l’utilisateur de cette clé privée.

Cliquer sur le bouton « save private key » pour la sauvegarder dans un dossier non partagé.

**IMPORTANT :** Faite une sauvegarde de cette clé privée dans une clé USB par exemple que vous mettrez à l’abri dans un autre lieu !

![Sauvegarde%20de%20la%20clé%20privée](https://aticdnet.albumhd.fr/r/11f46404-c78a-468d-bb4a-cda37ddebff4.png)

## Test et première utilisation de la clé SSH

Vous devez utiliser le programme pageant.

Depuis la zone de notification, vous devriez voir l’icône de pageant, faite un clique droit dessus et sélectionner « New session ».

![Création%20d\'une%20nouvelle%20session%20ssh](https://aticdnet.albumhd.fr/r/df669df9-119c-4362-89e7-4651ce9bce2f.png)

Depuis la partie gauche « Category », sélectionner la catégorie « Auth ».

Cliquer sur le bouton « Browse » et choisisser le fichier ppk contenant la clé privée que vous avez sauvegardé ci-dessus.

![Sélection%20de%20la%20clé%20privé%20SSH](https://aticdnet.albumhd.fr/r/72d5ad9f-b3ce-43f9-bab9-04ad0c6cd867.png)

Depuis la partie gauche « Category », revener sur la première catégorie « Session ».

Indiquer l’adresse IP du serveur linux précédée par {username} et séparée par @ : exemple root@1.2.3.4.

Indiquer le nom de la session dans la zone « Saved sessions » et cliquer sur Save.

![Création%20de%20la%20session%20SSH](https://aticdnet.albumhd.fr/r/1d253ef8-70f9-435b-8554-98541eff0a22.png)

Cliquer sur « Open », vous devriez à ce stade vous connecter après avoir saisi le mot de passe de la clé privé SSH que vous avez indiqué ci-dessus dans la zone passphrase (et non le mot de passe de l’utilisateur).
