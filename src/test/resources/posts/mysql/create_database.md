Pour ceux qui comme moi, créent rarement une nouvelle base de données, voici un rappel de quelques commandes utiles :

## Création de la base de données avec le charset UTF-8
        
```sql
CREATE DATABASE `DBNAME` CHARACTER SET utf8 COLLATE utf8_general_ci;				
```
        
## Création de l’utilisateur
        
```sql
CREATE USER `USERNAME`@`DOMAINE` IDENTIFIED BY "PASSWORD";				
```
        
## Gestion des droits
        
```sql
GRANT ALL PRIVILEGES ON `DBNAME`.* TO `USERNAME`@`DOMAINE` IDENTIFIED BY "PASSWORD";
FLUSH PRIVILEGES;				
```
