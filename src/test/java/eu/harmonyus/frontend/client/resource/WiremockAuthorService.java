package eu.harmonyus.frontend.client.resource;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import net.minidev.json.JSONObject;

import java.util.Collections;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WiremockAuthorService implements QuarkusTestResourceLifecycleManager {

    private WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {
        wireMockServer = new WireMockServer(options().dynamicPort());
        wireMockServer.start();
        WireMock wireMock = new WireMock(wireMockServer.port());
        addMockPostAuthor(wireMock);
        addMockGetAuthor(wireMock);
        return Collections.singletonMap("harmonyus.microservice.author/mp-rest/url", wireMockServer.baseUrl());
    }

    private void addMockGetAuthor(WireMock wireMock) {
        wireMock.register(get(urlEqualTo("/authors/thierry-counilh"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(getAuthor(true).toString())));
    }

    private void addMockPostAuthor(WireMock wireMock) {
        wireMock.register(post(urlEqualTo("/authors"))
                .withRequestBody(equalToJson(getAuthor(false).toString()))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")));
    }

    public static JSONObject getAuthor(boolean withCode)  {
        JSONObject obj = new JSONObject();
        if (withCode) {
            obj.put("code", "thierry-counilh");
        }
        obj.put("url", "http://author.harmonyus/api/v1/authors/thierry-counilh");
        obj.put("email", "thierry.counilh@aticdnet.com");
        obj.put("firstname", "Thierry");
        obj.put("lastname", "Counilh");
        return obj;
    }

    @Override
    public void stop() {
        if (null != wireMockServer) {
            wireMockServer.stop();
        }
    }
}
