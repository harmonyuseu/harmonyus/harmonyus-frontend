package eu.harmonyus.frontend.client.resource;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;
import java.util.Scanner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WiremockPostService implements QuarkusTestResourceLifecycleManager {

    private WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {
        wireMockServer = new WireMockServer(options().dynamicPort());
        wireMockServer.start();
        WireMock wireMock = new WireMock(wireMockServer.port());
        addMockPost(wireMock, getPostCreateDatabase());
        addMockPost(wireMock,getPostConnexionSSH());
        addMockPost(wireMock, getPostDisableRootLoginPassword());
        addMockGet(wireMock, "/posts?authorCode=&categoryCode=&tagCode=", getAll());
        addMockGet(wireMock, "/posts?authorCode=&categoryCode=&tagCode=ssh", getAllSSHTag());
        addMockGet(wireMock, "/posts?authorCode=&categoryCode=ubuntu&tagCode=", getAllSSHTag());
        addMockGet(wireMock, "/posts?authorCode=thierry-counilh&categoryCode=&tagCode=", getAll());
        addMockGet(wireMock, "mysql-creation-de-la-base-de-donnees-en-utilisant-le-charset-utf-8", getPostCreateDatabase());
        addMockGet(wireMock, "connexion-par-cle-ssh-sur-un-serveur-linux", getPostConnexionSSH());
        addMockGet(wireMock, "desactivation-du-login-par-mot-de-passe-sur-un-serveur-linux", getPostDisableRootLoginPassword());
        return Collections.singletonMap("harmonyus.microservice.post/mp-rest/url", wireMockServer.baseUrl());
    }

    private void addMockGet(WireMock wireMock, String code, JSONArray post) {
        wireMock.register(get(urlEqualTo(code))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(post.toString())));
    }

    private void addMockGet(WireMock wireMock, String code, JSONObject post) {
        wireMock.register(get(urlEqualTo("/posts/" + code))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(post.toString())));
    }

    private JSONArray getAll() {
        JSONArray jsonArray = new JSONArray();
        jsonArray.appendElement(getPostDisableRootLoginPassword());
        jsonArray.appendElement(getPostConnexionSSH());
        jsonArray.appendElement(getPostCreateDatabase());
        return jsonArray;
    }

    private JSONArray getAllSSHTag() {
        JSONArray jsonArray = new JSONArray();
        jsonArray.appendElement(getPostDisableRootLoginPassword());
        jsonArray.appendElement(getPostConnexionSSH());
        return jsonArray;
    }

    private void addMockPost(WireMock wireMock, JSONObject post) {
        wireMock.register(post(urlEqualTo("/posts"))
                .withRequestBody(equalToJson(post.toString()))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")));
    }

    private JSONObject getPostCreateDatabase() {
        JSONObject obj = new JSONObject();
        obj.put("author", WiremockAuthorService.getAuthor(true));
        obj.put("code", "mysql-creation-de-la-base-de-donnees-en-utilisant-le-charset-utf-8");
        obj.put("creation", "2020-12-13T18:10:28.569");
        obj.put("body", getBody("posts/mysql/create_database.md"));
        obj.put("title", "MySQL création de la base de données en utilisant le charset UTF-8");
        obj.put("category", WiremockCategoryService.getCategory("mysql", "mysql"));
        JSONArray objTags = new JSONArray();
        addTag(objTags, "create-database", "create database");
        addTag(objTags, "create-user", "create user");
        addTag(objTags, "grant", "grant");
        addTag(objTags, "privileges", "privileges");
        addTag(objTags, "sql", "sql");
        addTag(objTags, "utf8", "utf8");
        obj.put("tags", objTags);
        return obj;
    }

    private JSONObject getPostConnexionSSH() {
        JSONObject obj = new JSONObject();
        obj.put("author", WiremockAuthorService.getAuthor(true));
        obj.put("body", getBody("posts/ubuntu/connexion_ssh.md"));
        obj.put("code", "connexion-par-cle-ssh-sur-un-serveur-linux");
        obj.put("creation", "2020-12-13T18:10:28.569");
        obj.put("title", "Connexion par clé SSH sur un serveur Linux");
        obj.put("category", WiremockCategoryService.getCategory("ubuntu", "ubuntu"));
        JSONArray objTags = new JSONArray();
        addTag(objTags, "authentication", "authentication");
        addTag(objTags, "authorized_keys2", "authorized_keys2");
        addTag(objTags, "putty", "putty");
        addTag(objTags, "security", "security");
        addTag(objTags, "ssh", "ssh");
        addTag(objTags, "sshd_config", "sshd_config");
        obj.put("tags", objTags);
        return obj;
    }

    private JSONObject getPostDisableRootLoginPassword() {
        JSONObject obj = new JSONObject();
        obj.put("author", WiremockAuthorService.getAuthor(true));
        obj.put("body", getBody("posts/ubuntu/disable_root_login_password.md"));
        obj.put("code", "desactivation-du-login-par-mot-de-passe-sur-un-serveur-linux");
        obj.put("creation", "2020-12-13T18:10:28.569");
        obj.put("title", "Désactivation du login par mot de passe sur un serveur linux");
        obj.put("category", WiremockCategoryService.getCategory("ubuntu", "ubuntu"));
        JSONArray objTags = new JSONArray();
        addTag(objTags, "authentication", "authentication");
        addTag(objTags, "security", "security");
        addTag(objTags, "ssh", "ssh");
        addTag(objTags, "sshd_config", "sshd_config");
        obj.put("tags", objTags);
        return obj;
    }

    private Object getBody(String resourceName) {
        InputStream stream = getClass().getClassLoader().getResourceAsStream(resourceName);
        if (stream != null) {
            return new Scanner(stream, StandardCharsets.UTF_8).useDelimiter("\\A").next();
        }
        return null;
    }

    private void addTag(JSONArray objTags, String code, String name) {
        JSONObject obj = new JSONObject();
        obj.put("code", code);
        obj.put("name", name);
        obj.put("url", "http://tag.harmonyus/api/v1/tags/" + code);
        objTags.appendElement(obj);
    }

    @Override
    public void stop() {
        if (null != wireMockServer) {
            wireMockServer.stop();
        }
    }
}
