package eu.harmonyus.frontend.client.resource;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import net.minidev.json.JSONObject;

import java.util.Collections;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WiremockCategoryService implements QuarkusTestResourceLifecycleManager {

    private WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {
        wireMockServer = new WireMockServer(options().dynamicPort());
        wireMockServer.start();
        WireMock wireMock = new WireMock(wireMockServer.port());
        addMockPostCategory(wireMock, "ubuntu");
        addMockPostCategory(wireMock, "mysql");
        addMockGetCategory(wireMock, "ubuntu", "ubuntu");
        addMockGetCategory(wireMock, "mysql", "mysql");
        return Collections.singletonMap("harmonyus.microservice.category/mp-rest/url", wireMockServer.baseUrl());
    }

    private void addMockGetCategory(WireMock wireMock, String code, String name) {
        wireMock.register(get(urlEqualTo("/categories/" + code))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(getCategory(code, name).toString())));
    }

    private void addMockPostCategory(WireMock wireMock, String name) {
        wireMock.register(post(urlEqualTo("/categories"))
                .withRequestBody(equalToJson(getCategory(null, name).toString()))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")));
    }

    public static JSONObject getCategory(String code, String name) {
        JSONObject obj = new JSONObject();
        if (code != null) {
            obj.put("code", code);
            obj.put("url", "http://category.harmonyus/api/v1/categories/" + code);
        }
        obj.put("name", name);
        return obj;
    }

    @Override
    public void stop() {
        if (null != wireMockServer) {
            wireMockServer.stop();
        }
    }
}
