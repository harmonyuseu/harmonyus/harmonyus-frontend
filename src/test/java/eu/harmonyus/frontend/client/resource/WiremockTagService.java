package eu.harmonyus.frontend.client.resource;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import net.minidev.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WiremockTagService implements QuarkusTestResourceLifecycleManager {

    private WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {
        wireMockServer = new WireMockServer(options().dynamicPort());
        wireMockServer.start();
        WireMock wireMock = new WireMock(wireMockServer.port());
        List<String> allTags = Arrays.asList(
                "authentication",
                "authorized_keys2",
                "create database",
                "create user",
                "grant",
                "privileges",
                "putty",
                "security",
                "sql",
                "ssh",
                "sshd_config",
                "utf8");
        allTags.forEach(tag -> addMockPostTag(wireMock, tag));
        allTags.forEach(tag -> addMockGetTag(wireMock, tag));
        return Collections.singletonMap("harmonyus.microservice.tag/mp-rest/url", wireMockServer.baseUrl());
    }

    private void addMockPostTag(WireMock wireMock, String name) {
        wireMock.register(post(urlEqualTo("/tags"))
                .withRequestBody(equalToJson( getTag(null, name).toString()))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")));
    }

    private void addMockGetTag(WireMock wireMock, String name) {
        wireMock.register(get(urlEqualTo("/tags/" + name.replace(" ", "-")))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(getTag(name.replace(" ", "-"), name).toString())));
    }

    public static JSONObject getTag(String code, String name) {
        JSONObject obj = new JSONObject();
        if (code != null) {
            obj.put("code", code);
            obj.put("url", "http://tag.harmonyus/api/v1/tags/" + code);
        }
        obj.put("name", name);
        return obj;
    }

    @Override
    public void stop() {
        if (null != wireMockServer) {
            wireMockServer.stop();
        }
    }
}
