package eu.harmonyus.frontend.client.resource;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import net.minidev.json.JSONObject;

import java.util.Collections;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WiremockCoreService implements QuarkusTestResourceLifecycleManager {

    private WireMockServer wireMockServer;

    @Override
    public Map<String, String> start() {
        wireMockServer = new WireMockServer(options().dynamicPort());
        wireMockServer.start();
        WireMock wireMock = new WireMock(wireMockServer.port());
        addMockGetApplicationParameters(wireMock);
        return Collections.singletonMap("harmonyus.microservice.core/mp-rest/url", wireMockServer.baseUrl());
    }

    private void addMockGetApplicationParameters(WireMock wireMock) {
        JSONObject obj = new JSONObject();
        obj.put("title", "AtIcdNet");
        obj.put("version", "0.1.0");
        wireMock.register(get(urlEqualTo("/application-parameters"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody(obj.toString())));
    }

    @Override
    public void stop() {
        if (null != wireMockServer) {
            wireMockServer.stop();
        }
    }
}
