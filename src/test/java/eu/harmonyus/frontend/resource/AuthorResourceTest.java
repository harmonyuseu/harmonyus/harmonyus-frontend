package eu.harmonyus.frontend.resource;

import eu.harmonyus.frontend.client.resource.*;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

@QuarkusTest
@QuarkusTestResource(WiremockAuthorService.class)
@QuarkusTestResource(WiremockCategoryService.class)
@QuarkusTestResource(WiremockCoreService.class)
@QuarkusTestResource(WiremockPostService.class)
@QuarkusTestResource(WiremockTagService.class)
class AuthorResourceTest {

    @ConfigProperty(name = "quarkus.resteasy.path")
    String restPath;

    @Test
    @DisplayName("Test - When Calling GET - /author/${code} should return resource - 200")
    void testGetIndex() {
        String result = given()
                .when()
                .accept("text/html")
                .get(restPath + "author/thierry-counilh")
                .then()
                .statusCode(200)
                .extract().asPrettyString();
        Document document = Jsoup.parse(result);
        Assertions.assertEquals("AtIcdNet", document.title());
    }
}
