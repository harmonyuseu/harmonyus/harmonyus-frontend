package eu.harmonyus.frontend.resource;

import io.quarkus.qute.TemplateInstance;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/category")
public class CategoryResource extends CommonResource {

    @GET
    @Consumes(MediaType.TEXT_HTML)
    @Produces(MediaType.TEXT_HTML)
    @Path("{categoryCode}")
    public TemplateInstance post(@PathParam("categoryCode") String categoryCode) {
        return generateTemplate("", categoryCode, "");
    }
}
