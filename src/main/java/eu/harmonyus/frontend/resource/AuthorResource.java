package eu.harmonyus.frontend.resource;

import io.quarkus.qute.TemplateInstance;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/author")
public class AuthorResource extends CommonResource {

    @GET
    @Consumes(MediaType.TEXT_HTML)
    @Produces(MediaType.TEXT_HTML)
    @Path("{authorCode}")
    public TemplateInstance post(@PathParam("authorCode") String authorCode) {
        return generateTemplate(authorCode, "", "");
    }
}
