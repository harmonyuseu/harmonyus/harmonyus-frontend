package eu.harmonyus.frontend.resource;

import eu.harmonyus.frontend.client.model.ApplicationParameters;
import eu.harmonyus.frontend.client.model.Post;
import eu.harmonyus.frontend.client.service.ApplicationParametersService;
import eu.harmonyus.frontend.client.service.PostService;
import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;
import io.quarkus.qute.api.ResourcePath;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Inject;
import java.util.List;

public class CommonResource {

    @Inject
    @RestClient
    ApplicationParametersService applicationParametersService;

    @Inject
    @RestClient
    PostService postService;

    @Inject
    @ResourcePath("index")
    Template indexTemplate;

    TemplateInstance generateTemplate(String authorCode, String categoryCode, String tagCode) {
        Uni<ApplicationParameters> applicationParametersUni = applicationParametersService.getSystemInformation();
        Uni<List<Post>> postsUni = postService.getAll(authorCode, categoryCode, tagCode);
        return Uni
                .combine()
                .all()
                .unis(applicationParametersUni, postsUni)
                .asTuple()
                .map(objects -> indexTemplate
                        .data("applicationParameters", objects.getItem1())
                        .data("content", objects.getItem2().isEmpty() ? "error404" : "home")
                        .data("posts", objects.getItem2()))
                .await().indefinitely();
    }
}
