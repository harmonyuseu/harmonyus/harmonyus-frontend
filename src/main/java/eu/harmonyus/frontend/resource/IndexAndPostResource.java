package eu.harmonyus.frontend.resource;

import eu.harmonyus.frontend.client.model.ApplicationParameters;
import eu.harmonyus.frontend.client.model.Post;
import eu.harmonyus.frontend.client.service.ApplicationParametersService;
import eu.harmonyus.frontend.client.service.PostService;
import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;
import io.quarkus.qute.api.ResourcePath;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("{categoryCode:.*}")
public class IndexAndPostResource {

    @Inject
    @RestClient
    ApplicationParametersService applicationParametersService;

    @Inject
    @RestClient
    PostService postService;

    @Inject
    @ResourcePath("index")
    Template indexTemplate;

    @GET
    @Consumes(MediaType.TEXT_HTML)
    @Produces(MediaType.TEXT_HTML)
    public Uni<TemplateInstance> index() {
        Uni<ApplicationParameters> applicationParametersUni = applicationParametersService.getSystemInformation();
        Uni<List<Post>> postsUni = postService.getAll("", "", "");
        return generateTemplate(applicationParametersUni, "posts", postsUni, "home");
    }

    @GET
    @Consumes(MediaType.TEXT_HTML)
    @Produces(MediaType.TEXT_HTML)
    @Path("{postCode}")
    public Uni<TemplateInstance> post(@PathParam("postCode") String postCode) {
        Uni<ApplicationParameters> applicationParametersUni = applicationParametersService.getSystemInformation();
        Uni<Post> postUni = postService.getByCode(postCode.replaceAll(".html", ""));
        return generateTemplate(applicationParametersUni, "post", postUni, "post");
    }

    private Uni<TemplateInstance> generateTemplate(Uni<ApplicationParameters> applicationParametersUni, String dataName, Uni<?> data, String pageName) {
        return Uni
                .combine()
                .all()
                .unis(applicationParametersUni, data)
                .asTuple()
                .map(objects -> indexTemplate
                        .data("applicationParameters", objects.getItem1())
                        .data("content", pageName)
                        .data(dataName, objects.getItem2()))
                .onFailure().recoverWithItem(throwable -> {
                    throwable.printStackTrace();
                            return indexTemplate.data("applicationParameters", applicationParametersUni)
                                    .data("content", "error404");
                        }
                );

    }
}
