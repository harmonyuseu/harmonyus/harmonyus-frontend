package eu.harmonyus.frontend.resource;

import io.quarkus.qute.TemplateInstance;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/tag")
public class TagResource extends CommonResource {

    @GET
    @Consumes(MediaType.TEXT_HTML)
    @Produces(MediaType.TEXT_HTML)
    @Path("{tagCode}")
    public TemplateInstance post(@PathParam("tagCode") String tagCode) {
        return generateTemplate("", "", tagCode);
    }
}
