package eu.harmonyus.frontend.client.service;

import io.quarkus.arc.Unremovable;
import io.quarkus.runtime.annotations.RegisterForReflection;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
@RegisterForReflection
@Unremovable
public class TagProxyService {
    @Inject
    @RestClient
    TagService tagService;

    public TagService getTagService() {
        return tagService;
    }
}
