package eu.harmonyus.frontend.client.service;

import eu.harmonyus.frontend.client.model.Tag;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/tags")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey="harmonyus.microservice.tag")
public interface TagService {

    @GET
    Uni<List<Tag>> getAll();

    @GET
    @Path("/{code}")
    Uni<Tag> getByCode(@PathParam("code") String code);

    @POST
    Uni<Response> create(Tag tag);

    @PUT
    @Path("/{code}")
    Uni<Tag> modify(@PathParam("code") String code, Tag tag);

    @DELETE
    @Path("/{code}")
    Uni<Response> delete(@PathParam("code") String code);
}
