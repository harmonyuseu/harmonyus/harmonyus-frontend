package eu.harmonyus.frontend.client.service;

import eu.harmonyus.frontend.client.model.Author;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/authors")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey="harmonyus.microservice.author")
public interface AuthorService {

    @GET
    Uni<List<Author>> getAll();

    @GET
    @Path("/{code}")
    Uni<Author> getByCode(@PathParam("code") String code);

    @POST
    Uni<Response> create(Author author);

    @PUT
    @Path("/{code}")
    Uni<Author> modify(@PathParam("code") String code, Author author);

    @DELETE
    @Path("/{code}")
    Uni<Response> delete(@PathParam("code") String code);
}
