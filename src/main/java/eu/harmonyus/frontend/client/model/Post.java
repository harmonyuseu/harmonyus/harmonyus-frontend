package eu.harmonyus.frontend.client.model;

import eu.harmonyus.frontend.client.service.AuthorProxyService;
import eu.harmonyus.frontend.client.service.CategoryProxyService;
import eu.harmonyus.frontend.client.service.TagProxyService;
import io.quarkus.qute.TemplateData;
import io.quarkus.runtime.annotations.RegisterForReflection;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

import javax.enterprise.inject.spi.CDI;
import javax.json.bind.annotation.JsonbTransient;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RegisterForReflection
@TemplateData
public class Post {
    public String title;
    public String body;
    public String code;
    public String creation;
    public String modification;
    public UrlCode author;
    public UrlCode category;
    public List<UrlCode> tags;

    public Post() {
        this.author = new UrlCode();
        this.category = new UrlCode();
        this.tags = new ArrayList<>();
    }

    public String getAuthorFirstNameAndLastName() {
        return CDI.current().select(AuthorProxyService.class).get().getAuthorService().getByCode(author.getCode()).map(authorResult -> authorResult.firstname + " " + authorResult.lastname).await().indefinitely();
    }

    public String getCategoryName() {
        return CDI.current().select(CategoryProxyService.class).get().getCategoryService().getByCode(category.getCode()).map(categoryResult -> categoryResult.name).await().indefinitely();
    }

    public String bodySummary() {
        Parser parser = Parser.builder().build();
        Node document = parser.parse(body);
        HtmlRenderer renderer = HtmlRenderer.builder().build();
        String summary = renderer.render(document);
        summary = summary.replaceAll("<.*?>", "");
        if (summary.length() > 380) {
            return summary.substring(0, 380) + "...";
        }
        return summary;
    }

    public String bodyHTML() {
        Parser parser = Parser.builder().build();
        Node document = parser.parse(body);
        HtmlRenderer renderer = HtmlRenderer.builder().build();
        return renderer.render(document);
    }

    public String getTagName(String tagCode) {
        return CDI.current().select(TagProxyService.class).get().getTagService().getByCode(tagCode).map(tagResult -> tagResult.name).await().indefinitely();
    }

    public String formatDate(String date) {
        LocalDateTime localDateTime = LocalDateTime.parse(date, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        return ZonedDateTime.from(localDateTime.atZone(ZoneId.systemDefault())).format(DateTimeFormatter.ofPattern("dd LLLL y à H:m"));
    }

    public static class UrlCode {
        public String url;

        @JsonbTransient
        public String getCode() {
            return url.substring(url.lastIndexOf('/') + 1);
        }
    }
}
