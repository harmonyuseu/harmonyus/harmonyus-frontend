package eu.harmonyus.frontend.client.model;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class Author {
    public String firstname;
    public String lastname;
    public String code;
    public String email;
    public String description;

}
