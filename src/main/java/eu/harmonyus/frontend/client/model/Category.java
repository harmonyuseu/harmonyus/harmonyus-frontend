package eu.harmonyus.frontend.client.model;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class Category {
    public String name;
    public String code;
}
